#include "util.h"
#include <ctime>
#include <iterator>
#include <string>

// setting min and max year for birth date validation
time_t t = time(NULL);
tm* timePtr = localtime(&t);
//ctime year counts year 0 from 1900 hence + 1900
#define CURRENT_YEAR timePtr->tm_year + 1900
#define MIN_YEAR 1900

// free function used to make strings uppercase
std::string makeUppercase(std::string name)
{
    std::for_each(name.begin(), name.end(), [](char & c) {
        c = ::toupper(c);
    });
    return name;
}

// free function to check validity of birthdates
bool checkDate(int &d, int &m, int &y)
{
    //year, month and day range checks
    if ( (y < MIN_YEAR) || (y > CURRENT_YEAR) )
        return false;
    if ( m < 1 || m > 12)
        return false;
    if (d < 1 || d > 31)
        return false;
    
    //30 day month checks
    if ( m == 4 || m == 6 || m == 9 || m == 11)
        return (d <= 30);
    
    //leap year check
    if (m == 2)
    {
        if (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
            return (d <= 29);
        else
            return (d <= 28);
    }
    return true;
}

// free function to check validity of phone number
bool checkPhoneNum(std::string &number)
{
    if (number.size() != 11)
        return false;
    // string which contains all the legitimate characters (decimals 0-9) in a phone number
    const std::string allowedChars = "0123456789";
    
    for (auto charIterate = number.begin(); charIterate != number.end(); ++charIterate)
    {
        if (allowedChars.end() == std::find(allowedChars.begin(), allowedChars.end(), *charIterate))
        {
            return false;
        }
    }
    return true;
}
