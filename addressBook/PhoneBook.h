#ifndef _PHONEBOOK_H_
#define _PHONEBOOK_H_

#include <string>
#include <list>
#include <fstream>
#include "Contact.h"

class PhoneBook
{
public:
    PhoneBook();
    ~PhoneBook();
    // public methods
    void addContact();
    void deleteContact();
    void searchContacts();
    void printContacts();
    void readContact(std::string first, std::string last, std::string phone, int d, int m, int y);
    void readFromFile();
    void writeToFile();
private:
    //attributes
    std::list<Contact> phoneBook;
    // private methods
    void print(std::list<Contact>);
    void alphabeticalPrint(std::list<Contact>);
    void birthdayPrint(int&, int&, int&);
    void reassignIDNum();
    void idDelete(int id);
};
#endif // _PHONEBOOK_H_
