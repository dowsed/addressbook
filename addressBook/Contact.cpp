#include "Contact.h"
#include <iostream>

int Contact::ID = 0;

Contact::Contact()
    : firstName{""}, lastName{""}, phoneNum{""}, date{00}, month{00}, year{0000}
{
    idNum = ++ID;
}

Contact::Contact(std::string first, std::string last, std::string phone, int d, int m, int y)
    : firstName{first}, lastName{last}, phoneNum{phone}, date{d}, month{m}, year{y}
{
    idNum = ++ID;
}

Contact::~Contact()
{
}

//static methods

int Contact::getID()
{
    return ID;
}

void Contact::decrementID()
{
    --ID;
}

void Contact::resetID()
{
    ID = 0;
}

//getter methods

std::string Contact::getFullName() const
{
    std::string fullName = firstName + " " + lastName;
    return fullName;
}

std::string Contact::getFirstName() const
{
    return firstName;
}

std::string Contact::getLastName() const
{
    return lastName;
}

std::string Contact::getPhone() const
{
    return phoneNum;
}
int Contact::getDate() const
{
    return date;
}
int Contact::getMonth() const
{
    return month;
}
int Contact::getYear() const
{
    return year;
}

int Contact::getIDNum() const
{
    return idNum;
}

//setter methods

void Contact::setName(std::string first, std::string last)
{
    firstName = first;
    lastName = last;
}

void Contact::setPhone(std::string phone)
{
    phoneNum = phone;
}

void Contact::setDateOfBirth(int &d, int &m, int &y)
{
    date = d;
    month = m;
    year = y;
}

Contact Contact::setIDNum(int &num)
{
        this->idNum = num;
        return *this;
}

//print methods

void Contact::printName()
{
    std::cout << "Full name is: " << firstName << " " << lastName << std::endl;
}

void Contact::printPhone()
{
    std::cout << "Phone number is: " << phoneNum << std::endl;
}

void Contact::printDateOfBirth()
{
    std::cout << "Date of birth is: " << date << "/" << month << "/" << year << std::endl;
}

void Contact::printID()
{
    std::cout << "ID Number: " << idNum << std::endl;
}

bool Contact::checkDateOfBirth(int &d, int &m, int &y)
{
    if ( (d == getDate()) && (m == getMonth()) && (y == getYear()) )
        return true;
    else
        return false;
}
