#include "PhoneBook.h"
#include <iostream>
#include <iterator>
#include "util.h"

PhoneBook::PhoneBook()
{
    
}

PhoneBook::~PhoneBook()
{
    
}


void PhoneBook::addContact()
{
    //add first and last name, make entry all uppercase
    std::string first; std::string last;
    std::cout << "Please enter the contacts first name: " << std::endl;
    std::cin >> first;
    //clears cin buffer once new line is reached.
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Please enter the contacts surname name: " << std::endl;
    std::cin >> last;
    //clears cin buffer once new line is reached.
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    first = makeUppercase(first);
    last = makeUppercase(last);
    
    //add phone number and check validity
    bool validPhoneNumber = true;
    std::string phone;
    
    do {
    std::cout << "Please enter the contacts phone number: " << std::endl;
    std::cin >> phone;
    //clears cin buffer once new line is reached.
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    validPhoneNumber = checkPhoneNum(phone);
    if (!validPhoneNumber)
        std::cout << "Invalid phone number, please try again" << std::endl;
    } while (!validPhoneNumber);
    
    // add date of birth and check validity
    bool validDate = true;
    int d{00}; int m{00}; int y{0000};
    
    do {
    std::cout << "Please enter the contacts date (DD), month (MM) and year (YYYY) of birth separated by spaces: " << std::endl;
    std::cin >> d >> m >> y;
    //clears cin buffer once new line is reached.
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    validDate = checkDate(d, m, y);
    if (!validDate)
        std::cout << "Invalid date, please try again" << std::endl;
    } while (!validDate);
    Contact contact(first, last, phone, d, m, y);
    phoneBook.push_back(contact);
    std::cout << "Contact added succesfully" << std::endl;
}
void PhoneBook::deleteContact()
{
    if ( Contact::getID() == 0 )
    std::cout << "There are no contacts in the database, returning to menu \n";
    else
    {
        std::cout << "===============================================" << std::endl;
        std::cout << "Please choose an option" << std::endl;
        std::cout << "===============================================" << std::endl;
        std::cout << "A - Remove a single entry" << std::endl;
        std::cout << "B - Clear entire phone book" << std::endl;
        char option;
        std::cin >> option;
        option = toupper(option);
        if (option == 'A'){
            std::cout << "===============================================" << std::endl;
            print(this->phoneBook);
            int idRemove;
            do
            {
                std::cout << "Please type the ID of the contact to delete" << std::endl;
                std::cin >> idRemove;
                if (idRemove <= Contact::getID())
                {
                    idDelete(idRemove);
                    Contact::decrementID();
                    reassignIDNum();
                    break;
                }
                else
                {
                    std::cout << "Invalid ID, please try again" << std::endl;
                }
            } while (idRemove >= Contact::getID());
            

        }
        else if (option == 'B')
        {
            char option2;
            std::cout << "Are you sure you want to clear the entire phone book? y/n" << std::endl;
            std::cin >> option2;
            option2 = toupper(option2);
            if (option2 == 'Y'){
                phoneBook.clear();
                Contact::resetID();
                std::cout << "Phone book successfully cleared" << std::endl;
            }
            else if (option2 == 'N'){
                std::cout << "Returning to main menu" << std::endl;
            }
            else {
                std::cout << "Invalid selection" << std::endl;
            }
        }
        else
        {
            std::cout << "invalid selection" << std::endl;
        }
    }
}
void PhoneBook::searchContacts()
{
    if ( Contact::getID() == 0 )
    std::cout << "There are no contacts in the database, returning to menu \n";
    else
    {
        int numResults {0};
        std::string name;
        std::cout << "Please enter a name to search for: " << std::endl;
        std::cin >> name;
        name = makeUppercase(name);
        
        std::cout << "===============================================" << std::endl;
        std::list<Contact>::iterator iter;
        for (iter = phoneBook.begin(); iter != phoneBook.end(); iter++){
            if ((Contact(*iter)).getFullName().find(name) != std::string::npos){
                Contact(*iter).printName();
                Contact(*iter).printPhone();
                numResults++;
                std::cout << "===============================================" << std::endl;
            }
        }
        if (numResults == 0) {
            std::cout << "Name not found" << std::endl;
            std::cout << "===============================================" << std::endl;
        }
        else {
            std::cout << "Number of results: " << numResults << std::endl;
            std::cout << "===============================================" << std::endl;
        }
    }
}
void PhoneBook::printContacts()
{
    if ( Contact::getID() == 0 )
    std::cout << "There are no contacts in the database, returning to menu \n";
    else
    {
        std::cout << "===============================================" << std::endl;
        std::cout << "Please choose an option" << std::endl;
        std::cout << "===============================================" << std::endl;
        std::cout << "A - Print by ID" << std::endl;
        std::cout << "B - Print alphabetically by surname" << std::endl;
        std::cout << "C - Print matching birth dates" << std::endl;
        char option;
        std::cin >> option;
        option = toupper(option);
        if (option == 'A'){
            std::cout << "===============================================" << std::endl;
            print(this->phoneBook);
        }
        else if (option == 'B')
        {
            alphabeticalPrint(phoneBook);
        }
            
        else if (option == 'C')
        {
            int daySearch, monthSearch, yearSearch;
            bool validDate = true;
            do {
            std::cout << "please enter a birthdate you wish to search for separated by spaces DD MM YYYY" << std::endl;
            std::cin >> daySearch >> monthSearch >> yearSearch;
            validDate = checkDate(daySearch, monthSearch, yearSearch);
            if (!validDate)
                std::cout << "Invalid date, please try again" << std::endl;
            } while (!validDate);
            birthdayPrint(daySearch, monthSearch, yearSearch);
        }
        else
        {
            std::cout << "invalid selection" << std::endl;
        }
    }
}

void PhoneBook::reassignIDNum()
{
    std::list<Contact>::iterator iter;
    //re-assigning the IDs
       int index = 0;
       for (iter = phoneBook.begin(); iter != phoneBook.end(); ++iter){
           //creating an index based on the difference between the iterator and start of the list. Cast from long to int
           index = static_cast<int>(std::distance(phoneBook.begin(), iter)) + 1;
           // checks to see if the contacts ID is different to its new index position
           if ( Contact(*iter).getIDNum() != index)
           {
               //creates temporary object to store contact with the updated ID Num value so that it can be replaced in the list
               auto tempContact = Contact(*iter).setIDNum(index);
               //update list here
               *iter = tempContact;
           }
       }
}

void PhoneBook::idDelete(int id){
    std::list<Contact>::iterator iter;
    for (iter = phoneBook.begin(); iter != phoneBook.end(); ++iter){
        if (id == Contact(*iter).getIDNum()){
            //ID numbers start at 1 and list starts at position 0 so index is id-1
            phoneBook.erase(iter);
            std::cout << "ID No:" << id << ": " << Contact(*iter).getFullName() << " removed successfully." << std::endl;
        }
    }
}

void PhoneBook::print(std::list<Contact>printBook)
{
    std::cout << "===============================================" << std::endl;
    std::list<Contact>::iterator iter;
    for (iter = printBook.begin(); iter != printBook.end(); ++iter)
        {
            Contact(*iter).printID();
            Contact(*iter).printName();
            Contact(*iter).printPhone();
            Contact(*iter).printDateOfBirth();
            std::cout << "===============================================" << std::endl;
        }
}

void PhoneBook::alphabeticalPrint(std::list<Contact>sortedBook){
    std::cout << "Printing alphabetically surname, first name" << std::endl;
    sortedBook.sort([](Contact &contact1, Contact &contact2) {return contact1 < contact2;});
    print(sortedBook);
    
}

void PhoneBook::birthdayPrint(int &day, int &month, int &year){
    std::list<Contact>::iterator iter;
    int numMatches = 0;
    for (iter = phoneBook.begin(); iter != phoneBook.end(); ++iter)
    {
        if (Contact(*iter).checkDateOfBirth(day, month, year))
        {
            std::cout << "===============================================" << std::endl;
            std::cout << "Printing matching contact" << std::endl;
            Contact(*iter).printID();
            Contact(*iter).printName();
            Contact(*iter).printPhone();
            Contact(*iter).printDateOfBirth();
            std::cout << "===============================================" << std::endl;
            ++numMatches;
        }
    }
    if (numMatches == 0)
    {
        std::cout <<"No matches found" << std::endl;
        std::cout << "===============================================" << std::endl;
    }
}

void PhoneBook::readContact(std::string first, std::string last, std::string phone, int d, int m, int y)
{
    Contact contact(first, last, phone, d, m, y);
    phoneBook.push_back(contact);
}

void PhoneBook::readFromFile()
{
    std::ifstream inputFile;
    inputFile.open("phoneBook.txt"); // works with Xcode please check folder extensions on other IDEs
    if (!inputFile){
        std::cerr << "Problem opening file, if you haven't already created a phoneBook database it will be created automatically when you quit the program. If you have created a phoneBook database please check that the phoneBook.txt is located in the same folder as the executable." << std::endl;
    }
    std::string first; std::string last; std::string phone; int d; int m; int y;
    while (inputFile >> first >> last >> phone >> d >> m >> y){
        readContact(first, last, phone, d, m, y);
    }
    inputFile.close();
}

void PhoneBook::writeToFile()
{
    std::ofstream outputFile;
    outputFile.open("phoneBook.txt"); // like inputFile check IDE for correct folder extension
    if (!outputFile){
        std::cerr << "Problem loading output file, please check that the phoneBook.txt is located in the same folder as the executable" << std::endl;
    }
    std::list<Contact>::iterator iter;
    std::string first; std::string last; std::string phone; int d{0}; int m{0}; int y{0};
    for (iter = phoneBook.begin(); iter != phoneBook.end(); iter++){
        first = Contact(*iter).getFirstName();
        last = Contact(*iter).getLastName();
        phone = Contact(*iter).getPhone();
        d = Contact(*iter).getDate();
        m = Contact(*iter).getMonth();
        y = Contact(*iter).getYear();
        
        outputFile << " " << first << " " << last << " "<< phone << " " << d << " " << m << " "<< y;
    }
    
    outputFile.close();
}
