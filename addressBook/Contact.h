#ifndef _CONTACT_H_
#define _CONTACT_H_

#include <string>

class Contact
{
public:
    Contact();
    Contact(std::string first, std::string last, std::string phone, int d, int m, int y);
    
    ~Contact();
    
    //static methods
    static void decrementID();
    static void resetID();
    static int getID();
    
    //getter methods
    std::string getFullName() const;
    std::string getFirstName() const;
    std::string getLastName() const;
    std::string getPhone() const;
    int getDate() const;
    int getMonth() const;
    int getYear() const;
    int getIDNum() const;
    
    //setter methods
    void setName(std::string first, std::string last);
    void setPhone(std::string phone);
    void setDateOfBirth(int& d, int& m, int& y);
    Contact setIDNum(int& num);
    
    //print methods
    void printName();
    void printPhone();
    void printDateOfBirth();
    void printID();
    
    //comparison method
    bool checkDateOfBirth(int& d, int& m, int& y);
    
    //comparison operator overloaded function
    bool operator< (const Contact& other)
    {
        //compares surnames
        if (this-> getLastName() < other.getLastName() )
        {
            return true;
        }
        //compares first names when surnames are equal
        else if ( (this->getLastName() == other.getLastName() ) && (this->getFirstName() < other.getFirstName()) )
        {
            return true;
        }
        //catch all
        else
        {
            return false;
        }
    }
    
private:
    //attributes
    static int ID; // static int used to increment each new contacts ID number
    int idNum; // stored unique ID number for each contact
    std::string firstName;
    std::string lastName;
    std::string phoneNum;
    int date;
    int month;
    int year;

    
    
};

#endif // _CONTACT_H_
