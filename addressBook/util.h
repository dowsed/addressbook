#ifndef util_h
#define util_h

#include <ctime>
#include <string>


// free function used to make strings uppercase
std::string makeUppercase(std::string);

// free function to check validity of birthdates
bool checkDate(int&, int&, int&);

// free function to check validity of phone number
bool checkPhoneNum(std::string&);

#endif /* util_h */
